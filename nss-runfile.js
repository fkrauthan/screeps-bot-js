const { task, runTask, run, taskGroup } = require("nss-run");
const rimraf = require("rimraf");
const path = require("path");
const fs = require("fs");
const os = require("os");
const { mkdir } = require("mkdir-recursive");
const { promisify } = require("util");

const rimrafAsync = promisify(rimraf);
const mkdirAsync = promisify(mkdir);
const readdir = promisify(fs.readdir);
const stat  = promisify(fs.stat);
const lstat = promisify(fs.lstat);

const SRC_DIRECTORY = "./src";
const DIST_DIRECTORY = "./dist";

async function getFiles(dir, extension) {
    const subdirs = await readdir(dir);
    const files = await Promise.all(subdirs.map(async (subdir) => {
        const res = path.resolve(dir, subdir);
        return (await stat(res)).isDirectory() ? getFiles(res, extension) : res;
    }));
    return files
        .reduce((a, f) => a.concat(f), [])
        .filter((file) => path.extname(file) === extension);
}

async function isDirectory(path) {
    try {
        return (await stat(path)).isDirectory();
    } catch (e) {
        return false;
    }
}

async function deployToSteam(server, branch) {
	function formatScreepsServerName(server) {
		return server
			.replace(/\./g, '_')
			.replace(/:/g, '___');
	}
	function detectScreepsPath() {
		if (process.platform === 'win32') {
			return path.join(os.homedir(), 'AppData', 'Local', 'Screeps', 'scripts', formatScreepsServerName(server), branch);
		}
		if (process.platform === 'darwin') {
			return path.join(os.homedir(), 'Library', 'Application Support', 'Screeps', 'scripts', formatScreepsServerName(server), branch);
		}

		throw new Error(`${process.platform} not supported at the moment!`);
	}

	const destinationPath = detectScreepsPath();
	try {
		await lstat(destinationPath);
	} catch (e) {
		await mkdirAsync(destinationPath);
	}

	await run(`sync-files "${DIST_DIRECTORY}" "${destinationPath}"`);
}

task("clean", () => rimrafAsync(DIST_DIRECTORY));

task("lint", () => run(`eslint ${SRC_DIRECTORY}`));

task("build", async () => {
    if (!(await isDirectory(DIST_DIRECTORY))) {
		await (mkdirAsync(DIST_DIRECTORY));
    }

    const sourceRoot = path.join(process.cwd(), SRC_DIRECTORY);
    const sourceFiles = await getFiles(SRC_DIRECTORY, ".js");
    await Promise.all(sourceFiles.map((file) => {
        const filePath = path.parse(file);
        let outputPath = filePath.base;

        const relativePath = path.relative(sourceRoot, filePath.dir);
        if (relativePath !== "") {
            outputPath = `${relativePath.split(path.sep).join("__")}__${filePath.base}`;
        }
        return run(`babel "${file}" --out-file="${path.join(DIST_DIRECTORY, outputPath)}"`)
    }))
});

task("deploy", async () => {
    await runTask("build");

    const screepsJson = require("./screeps.json");
    const server = process.env.SCREEPS_SERVER || "local";
    if (!screepsJson[server]) {
        throw new Error(`Could not find server "${server}" configuration in screeps.json`);
    }

    const config = screepsJson[server];
    if (config.steam) {
        await deployToSteam(config.server, config.branch);
    } else {
		throw new Error(`Could not deploy to server "${server}": Non steam deployments are not implemented yet!`);
    }
});
taskGroup("cleanAndDeploy", ["clean", "deploy"]);
