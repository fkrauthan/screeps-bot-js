module.exports = function (api) {
    api.cache(true);

    const presets = [
        [
            "@babel/env",
            {
                "targets": {
                    "node": "8.9"
                },
            },
        ],
    ];
    const plugins = [
        "@babel/plugin-proposal-class-properties",
        "babel-plugin-transform-inline-environment-variables",
        "babel-plugin-minify-constant-folding",
        ["./babel/babel-plugin-screeps-flatten-modules", {
            "root": "./src",
        }],
    ];

    return {
        presets,
        plugins,
    };
};
