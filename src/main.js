import "./navigation/extensions";

import Kernel from "./os/Kernel";
import {onlyOneTickAdvanced} from "./utils/memory";
import defaultProcessTypes from "./os/programs/factory";
import coreProcessTypes from "./core/programs/factory";

export function loop() {
	const didAdvanceOnlyOneTick = onlyOneTickAdvanced();

	// Initialize Kernel
	/** @type {Kernel} kernel */
	const kernel = (global.kernel && didAdvanceOnlyOneTick) ? global.kernel : new Kernel();
	global.kernel = kernel;

	// Register bundles if new kernel
	// if (kernel.new) {
		kernel.registerProcessTypes(defaultProcessTypes);
		kernel.registerProcessTypes(coreProcessTypes);
	// }
	kernel.addProcessWithPIDIfNotExist("init", 1, 99); // TODO <- Remove this and make Kernel ensure that init is always running

	// Run the kernel
	kernel.boot();

	console.log(`[${Game.time}] Execution Stats: ${JSON.stringify(kernel.stats)}`)
}
