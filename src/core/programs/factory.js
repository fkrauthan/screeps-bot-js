import InitProcess, { TYPE as TYPE_INIT} from "./InitProcess";
import RoomMasterProcess, { TYPE as TYPE_ROOM_MASTER} from "./RoomMasterProcess";
import UpgradeRoleProcess, { TYPE as TYPE_UPGRADE_ROLE} from "./UpgradeRoleProcess";

export default {
    [TYPE_INIT]: InitProcess,
    [TYPE_ROOM_MASTER]: RoomMasterProcess,

    [TYPE_UPGRADE_ROLE]: UpgradeRoleProcess,
};

