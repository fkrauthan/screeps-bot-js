import Process from "../../os/Process";

export const TYPE = "init";
export default class InitProcess extends Process {

    /**
     * @param {ProcessPrototype} proto
     * @param {Kernel} kernel
     */
    constructor(proto, kernel) {
        super(TYPE, proto, kernel);
    }

    run() {
        // Start memory cleanup job
        if (this.data.cleanupPID === undefined) {
            this.data.cleanupPID = this.fork("creepMemoryClean", 1);
        }

        // Start room master process for each room
        Object.values(Game.rooms)
            .filter(({ controller }) => controller && controller.my)
            .filter((room) => room.memory._pid === undefined || !this.kernel.hasProcess(room.memory._pid))
            .forEach((room) => room.memory._pid = this.fork('roomMaster', 50, { room: room.name }));

        // No need to run task all the time
        this.suspendForTicks(10);
    }

}
