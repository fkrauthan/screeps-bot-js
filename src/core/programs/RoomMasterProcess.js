import Process from "../../os/Process";

export const TYPE = "roomMaster";
export default class RoomMasterProcess extends Process {

    /**
     * @param {ProcessPrototype} proto
     * @param {Kernel} kernel
     */
    constructor(proto, kernel) {
        super(TYPE, proto, kernel);

        this.room = Game.rooms[this.data.room];
        this.controller = this.room.controller;
        this.spawn = Object
            .values(Game.spawns)
            .filter(({ room }) => room.name === this.data.room)
            .reduce((acc, spawn) => spawn);

        if (!this.data.sourceIds) {
            this.data.sourceIds = this.room.find(FIND_SOURCES).map((source) => source.id);
            this.data.nextSource = 0;
        }
    }

    run() {
        // Spawn new creep
        if (this.spawn.spawnCreep([CARRY, WORK, MOVE], `J${Game.time}`) === OK) {
            console.log(`Spawned new basic creep J${Game.time}`);

            this.suspendForTicks(9);
        } else {
            this.suspendForTicks(this.spawn.energyCapacity - this.spawn.energy);
        }

        // Assign required roles
        this.room.find(FIND_MY_CREEPS)
            .filter((creep) => creep.memory._pid === undefined || !this.kernel.hasProcess(creep.memory._pid))
            .forEach((creep) => {
                creep.memory._pid = this.fork('upgradeRole', 20, {
                    creep: creep.name,
                    sourceId: this.data.sourceIds[this.data.nextSource],
                });
                this.data.nextSource += 1;
                if (this.data.nextSource >= this.data.sourceIds.length) {
                    this.data.nextSource = 0;
                }
            });
    }

}
