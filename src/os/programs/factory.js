import CreepMemoryCleanProcess, { TYPE as TYPE_CREEP_MEMORY_CLEAN } from "./CreepMemoryCleanProcess";
import MoveProcess, { TYPE as TYPE_MOVE } from "./MoveProcess";
import BuildProcess, { TYPE as TYPE_BUILD } from "./BuildProcess";
import HarvestProcess, { TYPE as TYPE_HARVEST } from "./HarvestProcess";
import UpgradeProcess, { TYPE as TYPE_UPGRADE } from "./UpgradeProcess";

export default {
    [TYPE_CREEP_MEMORY_CLEAN]: CreepMemoryCleanProcess,
    [TYPE_MOVE]: MoveProcess,
    [TYPE_BUILD]: BuildProcess,
    [TYPE_HARVEST]: HarvestProcess,
    [TYPE_UPGRADE]: UpgradeProcess,
};
