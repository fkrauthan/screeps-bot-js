import SimpleCreepProcess from "./helper/SimpleCreepProcess";

export const TYPE = "harvest";
export default class HarvestProcess extends SimpleCreepProcess {

    /**
     * @param {ProcessPrototype} proto
     * @param {Kernel} kernel
     */
    constructor(proto, kernel) {
        super(TYPE, proto, kernel, {});
    }

    isValidProcess(creep) {
        return _.sum(creep.carry) < creep.carryCapacity;
    }

    isValidTarget(creep, target) {
        return target.energy !== undefined ? target.energy > 0 : target.mineralAmount > 0;
    }

    work(creep, target) {
        return creep.harvest(this.target);
    }

}
