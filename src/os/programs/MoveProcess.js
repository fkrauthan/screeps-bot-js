import CreepProcess from "./helper/CreepProcess";
import {deref, derefRoomPosition} from "../../utils/target";

export const TYPE = "move";
export default class MoveProcess extends CreepProcess {

    /**
     * @param {ProcessPrototype} proto
     * @param {Kernel} kernel
     */
    constructor(proto, kernel) {
        super(TYPE, proto, kernel);
    }

    run() {
        const creep = this.creep;
        if (!creep) {
            this.finish();
        }

        if(creep.spawning) {
            return;
        }

        let targetPos = this.data.target.ref ? deref(this.data.target.ref).pos : derefRoomPosition(this.data.target.pos);
        if(creep.pos.inRangeTo(targetPos, this.data.range)) {
            if (this.data.workOffRoad) {
                creep.park(targetPos, true);
            }

            this.finish();
            return;
        }

        if (creep.fatigue === 0) {
            creep.travelTo(targetPos, {
                range: this.data.range,
            });
        } else {
            let decreasePerTick = creep.body.reduce((acc, { type }) => acc + (type === MOVE ? 2 : 0), 0);
            this.suspendForTicks(Math.ceil(creep.fatigue / decreasePerTick));
        }
    }

}
