import CreepProcess from "./CreepProcess";
import {deref} from "../../../utils/target";

/**
 * @abstract
 */
export default class SimpleCreepProcess extends CreepProcess {

    /**
     * @param {string} type
     * @param {ProcessPrototype} protoType
     * @param {Kernel} kernel
     * @param {number} targetRange
     * @param {boolean} workOffRoad
     * @param {boolean} targetStationary
     * @param {boolean} oneShot
     */
    constructor(type, protoType, kernel, { targetRange = 1, workOffRoad = false, targetStationary = true, oneShot = false }) {
        super(type, protoType, kernel);

        this.targetRange = targetRange;
        this.targetStationary = targetStationary;
        this.workOffRoad = workOffRoad;
        this.oneShot = oneShot;
    }

    /**
     * @return {RoomObject | null}
     */
    get target() {
        return deref(this.data.target);
    }

    /**
     * @param {Creep} creep
     * @param {RoomObject | null} target
     * @return {boolean}
     *
     * @abstract
     */
    isValidTarget(creep, target) {
        return true;
    }

    /**
     * @param {Creep} creep
     * @param {RoomObject | null} target
     * @return {boolean}
     *
     * @abstract
     */
    isValidProcess(creep, target) {
        return true;
    }

    /**
     * @param {Creep} creep
     * @param {RoomObject | null} target
     * @return {ScreepsReturnCode}
     *
     * @abstract
     */
    work(creep, target) {
        return ERR_BUSY;
    }

    run() {
        const creep = this.creep;
        if (!creep) {
            this.finish();
            return;
        }

        const target = this.target;
        if (!target) {
            this.finish();
            return;
        }

        if (!this.isValidTarget(creep, target) || !this.isValidProcess(creep, target)) {
            this.finish();
            return;
        }

        if (!this.creep.pos.inRangeTo(target.pos, this.targetRange) || this.creep.pos.isEdge) {
            this.forkMoveTo(this.targetStationary ? target.pos : target, this.targetRange, this.workOffRoad);
            return;
        }

        let result = this.work(creep, target);
        if (this.oneShot && result === OK) {
            this.finish();
        }
    }

}
