import Process from "../../Process";
import {refTarget} from "../../../utils/target";

/**
 * @abstract
 */
export default class CreepProcess extends Process {

    /**
     * @param {string} type
     * @param {ProcessPrototype} protoType
     * @param {Kernel} kernel
     */
    constructor(type, protoType, kernel) {
        super(type, protoType, kernel);
    }

    /**
     * @return {boolean}
     */
    isAlife() {
        return !!Game.creeps[this.data.creep];
    }

    /**
     * @return {Creep}
     */
    get creep() {
        return Game.creeps[this.data.creep];
    }

    forkMoveTo(target, range = 1, workOffRoad = false) {
        this.forkAndSuspend("move", this.priority + 1, {
            creep: this.creep.name,
            range,
            target: refTarget(target),
            workOffRoad,
        })
    }
}