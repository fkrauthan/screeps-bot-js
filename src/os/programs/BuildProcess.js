import SimpleCreepProcess from "./helper/SimpleCreepProcess";

export const TYPE = "build";
export default class BuildProcess extends SimpleCreepProcess {

    /**
     * @param {ProcessPrototype} proto
     * @param {Kernel} kernel
     */
    constructor(proto, kernel) {
        super(TYPE, proto, kernel, {
            targetRange: 3,
            workOffRoad: true,
        });
    }

    isValidProcess(creep, target) {
        return creep.carry.energy > 0;
    }

    isValidTarget(creep, target) {
        return target.my && target.progress < target.progressTotal;
    }

    work(creep, target) {
        return creep.build(target);
    }

}
