import SimpleCreepProcess from "./helper/SimpleCreepProcess";

export const TYPE = "upgrade";
export default class UpgradeProcess extends SimpleCreepProcess {

    /**
     * @param {ProcessPrototype} proto
     * @param {Kernel} kernel
     */
    constructor(proto, kernel) {
        super(TYPE, proto, kernel, {
            targetRange: 3,
            workOffRoad: true,
        });
    }

    isValidProcess(creep) {
        return creep.carry.energy > 0;
    }

    isValidTarget(creep, target) {
        return target.my;
    }

    work(creep, target) {
        return creep.upgradeController(target);
    }

}
