/**
 * @typedef {object} ProcessPrototype
 * @property {string} type
 * @property {number} pid
 * @property {number} priority
 * @property {number} lastExecution
 * @property {number | boolean} suspended
 * @property {?number} suspendedForPID
 * @property {{}} data
 * @property {?string} parent
 */

/**
 * @property {string} type
 * @property {?string} tag
 * @property {number} pid
 * @property {Kernel} kernel
 * @property {number} priority
 */
export default class Process {

    /**
     * @type {number}
     */
    lastExecution = -1;

    /**
     * @type {boolean}
     */
    completed = false;

    /**
     * @type {string | number | boolean}
     */
    suspended = false;

    /**
     * @type {?number}
     */
    suspendedForPID = undefined;

    /**
     * @type {{}}
     */
    data = {};

    /**
     * @type {?Process}
     */
    parent = undefined;

    /**
     * @param {string} type
     * @param {ProcessPrototype} protoType
     * @param {Kernel} kernel
     */
    constructor(type, protoType, kernel) {
        this.type = type;
        this.kernel = kernel;

        this.pid = protoType.pid;
        this.tag = protoType.tag;
        this.priority = protoType.priority;
        this.suspended = protoType.suspended || false;
        this.suspendedForPID = protoType.suspendedForPID;
        this.data = protoType.data || {};
        this.lastExecution = protoType.lastExecution || -1;

        this.proto = protoType;
        if (protoType.parent) {
            this.parent = kernel.getProcess(protoType.parent);
        }
    }

    /**
     * @abstract
     */
    run() {
        this.completed = true;
    }

    /**
     * @param {string} type
     * @param {number} priority
     * @param {?{}} data
     * @param {?string} tag
     *
     * @return {number}
     */
    fork(type, priority, data = {}, tag = undefined) {
        return this.kernel.addProcess(type, priority, data, { parent: this.pid, tag, });
    }

    /**
     * @param {string} type
     * @param {number} priority
     * @param {?{}} data
     * @param {?string} tag
     *
     * @return {number}
     */
    forkAndSuspend(type, priority, data = {}, tag = undefined) {
        const pid = this.fork(type, priority, data, tag);
        this.suspendForPid(pid);

        return pid;
    }

    finish(resumeParent = true) {
        this.completed = true;

        if (resumeParent && this.parent) {
            this.parent.resume();
        }
    }

    /**
     * @return {Process[]}
     */
    getChildProcesses() {
        return this.kernel.getChildProcesses(this.pid);
    }

    /**
     * @param {string} type
     * @return {boolean}
     */
    hasChildProcessWithType(type) {
        return this.getChildProcesses()
            .findIndex((process) => process.type === type) !== -1;
    }

    /**
     * @param {number} pid
     */
    suspendForPid(pid) {
        this.suspendedForPID = pid;
        this.suspended = true;
    }

    /**
     * @param {number} ticks
     */
    suspendForTicks(ticks = 1) {
        this.suspended = ticks + 1;
    }

    resume() {
        this.suspended = false;
        this.suspendedForPID = undefined;
    }

    /**
     * @return {ProcessPrototype}
     */
    get prototype() {
        this.proto.pid = this.pid;
        this.proto.priority = this.priority;
        this.proto.suspended = this.suspended;
        this.proto.suspendedForPID = this.suspendedForPID;
        this.proto.lastExecution = this.lastExecution;
        this.proto.data = this.data;
        this.proto.parent = this.parent ? this.parent.pid : undefined;

        return this.proto;
    }
}
