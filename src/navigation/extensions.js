import Traveler from "./traveler";

// Creep extensions
Creep.prototype.travelTo = function (destination, options) {
    return Traveler.travelTo(this, destination, options);
};

Creep.prototype.park = function (pos, maintainDistance = false) {
    return Traveler.park(this, pos, maintainDistance);
};

// RoomPosition extensions
Object.defineProperty(RoomPosition.prototype, 'isEdge', {
    get: function () {
        return this.x === 0 || this.x === 49 || this.y === 0 || this.y === 49;
    },
});


