
/**
 * @return {boolean}
 */
export function onlyOneTickAdvanced() {
	let res = false;
	if(global.lastTick && Game.time === (global.lastTick + 1)) {
		res = true;
	}
	global.lastTick = Game.time;
	return res;
}
