/**
 * @param {string} ref
 * @return {RoomObject | null}
 */
export function deref(ref) {
	return Game.getObjectById(ref) || Game.flags[ref] || Game.creeps[ref] || Game.spawns[ref] || null;
}

/**
 * @param {object} protoPos
 * @return {RoomPosition}
 */
export function derefRoomPosition(protoPos) {
	return new RoomPosition(protoPos.x, protoPos.y, protoPos.roomName);
}

export function refTarget(ref) {
	if (ref.pos) {
		return {
			ref: ref.ref,
			pos: {
				x: ref.pos.x,
				y: ref.pos.y,
				roomName: ref.pos.roomName,
			},
		};
	} else {
		return {
			ref: undefined,
			pos: {
				x: ref.x,
				y: ref.y,
				roomName: ref.roomName,
			},
		}
	}
}
