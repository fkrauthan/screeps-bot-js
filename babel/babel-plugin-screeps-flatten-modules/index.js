const transformCall = require("./transformers/call");
const transformImport = require("./transformers/import");

const importVisitors = {
    CallExpression: transformCall,
    "ImportDeclaration|ExportDeclaration": transformImport,
};

const visitor = {
    Program: {
        enter(programPath, state) {
            programPath.traverse(importVisitors, state);
        },
        exit(programPath, state) {
            programPath.traverse(importVisitors, state);
        },
    },
};

module.exports = ({ types }) => ({
    name: "screeps-flatten-modules",

    pre() {
        this.types = types;
        this.moduleResolverVisited = new Set();
    },

    visitor,

    post() {
        this.moduleResolverVisited.clear();
    },
});
