const path = require("path");
const fs = require("fs");

function isRelativePath(nodePath) {
    return nodePath.match(/^\.?\.\//) != null;
}

function isFolder(nodePath) {
    try {
        return fs.statSync(nodePath).isDirectory();
    } catch (e) {
        return false;
    }
}

function isFile(nodePath) {
    try {
        return fs.statSync(`${nodePath}.js`).isFile();
    } catch (e) {
        return false;
    }
}

function findSourceRoot(opts) {
    let sourceRoot = opts.root || process.cwd();
    if (isRelativePath(sourceRoot)) {
        sourceRoot = path.join(process.cwd(), sourceRoot);
    }
    return sourceRoot;
}

function resolvePath(sourcePath, currentFile, opts) {
    const sourceRoot = findSourceRoot(opts);

    const currentFilePath = path.parse(currentFile);
    if (isRelativePath(sourcePath)) {
        sourcePath = path.join(currentFilePath.dir, sourcePath);
    }

    if (isFolder(sourcePath)) {
        sourcePath = path.join(sourcePath, "index");
    }
    if (!isFile(sourcePath)) {
        console.log(`Could not resolve "${sourcePath}" in file ${currentFile}.`);
        return;
    }

    const relativePath = path.relative(sourceRoot, sourcePath);
    if (relativePath.startsWith("..")) {
        console.log(`Could not resolve "${sourcePath}" in file ${currentFile}. Module seem to be outside of source root!`);
        return;
    }

    return relativePath.split(path.sep).join("__");
}

module.exports = {

    mapPathString: function (nodePath, state) {
        if (!state.types.isStringLiteral(nodePath)) {
            return;
        }

        const sourcePath = nodePath.node.value;
        const currentFile = state.file.opts.filename;

        const modulePath = resolvePath(sourcePath, currentFile, state.opts);
        if (modulePath) {
            if (nodePath.node.pathResolved) {
                return;
            }

            nodePath.replaceWith(state.types.stringLiteral(modulePath));
            nodePath.node.pathResolved = true;
        }
    },

    isImportCall: function (types, calleePath) {
        return types.isImport(calleePath.node.callee);
    },

    matchesPattern: function (types, calleePath, pattern) {
        const { node } = calleePath;

        if (types.isMemberExpression(node)) {
            return calleePath.matchesPattern(pattern);
        }
        if (!types.isIdentifier(node) || pattern.includes('.')) {
            return false;
        }

        const name = pattern.split('.')[0];
        return node.name === name;
    }

};