const { mapPathString, isImportCall, matchesPattern } = require("../utils");

const importFunctions = [
    'require',
    'require.resolve',
    'System.import',
];
module.exports = function transformCall(nodePath, state) {
    if (state.moduleResolverVisited.has(nodePath)) {
        return;
    }

    const calleePath = nodePath.get("callee");

    const isNormalCall = importFunctions.some(
        pattern => matchesPattern(state.types, calleePath, pattern),
    );

    if (isNormalCall || isImportCall(state.types, nodePath)) {
        state.moduleResolverVisited.add(nodePath);
        mapPathString(nodePath.get('arguments.0'), state);
    }
};
