const { mapPathString } = require("../utils");

module.exports = function transformCall(nodePath, state) {
    if (state.moduleResolverVisited.has(nodePath)) {
        return;
    }
    state.moduleResolverVisited.add(nodePath);

    mapPathString(nodePath.get('source'), state);
};
