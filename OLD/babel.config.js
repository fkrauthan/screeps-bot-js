module.exports = function (api) {
    api.cache(true);

    const presets = [
        [
            "@babel/env",
            {
                "targets": {
                    "node": "8.9"
                },
            },
        ],
    ];
    const plugins = [
        "@babel/plugin-proposal-class-properties",
    ];

    return {
        presets,
        plugins,
    };
};
