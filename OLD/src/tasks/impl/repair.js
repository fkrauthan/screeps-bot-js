import Task from "../task";

export const NAME = "repair";

/**
 * @property {Structure} target
 */
export default class RepairTask extends Task {

    /**
     * @param {Structure} target
     * @param {TaskOptions} options
     */
    constructor(target, options = {}) {
        super(NAME, target, options);

        this.settings.timeout = 100;
        this.settings.targetRange = 3;
    }

    isValidTask() {
        return this.creep.carry.energy > 0;
    }

    isValidTarget() {
        return this.target && this.target.hits < this.target.hitsMax;
    }

    work() {
        let result = this.creep.repair(this.target);
        if (this.target.structureType === STRUCTURE_ROAD) {
            const newHits = this.target.hits + this.creep.getActiveBodyparts(WORK) * REPAIR_POWER;
            if (newHits > this.target.hitsMax) {
                this.finish();
            }
        }
        return result;
    }

}
