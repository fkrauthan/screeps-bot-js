import Task from "../task";

export const NAME = "transfer";

function isEnergyStructure(structure) {
    return structure.energy !== undefined && structure.energyCapacity !== undefined;
}

function isStoreStructure(structure) {
    return structure.store !== undefined;
}

/**
 * @property {StructureStorage | StructureTerminal | StructureExtension | StructureContainer | StructureLab | StructureNuker | StructurePowerSpawn | Creep} target
 * @property {ResourceConstant} data.resourceType
 * @property {?number} data.amount
 */
export default class TransferTask extends Task {

    /**
     * @param {StructureStorage | StructureTerminal | StructureExtension | StructureContainer | StructureLab | StructureNuker | StructurePowerSpawn | Creep} target
     * @param {ResourceConstant} resourceType
     * @param {?number} amount
     * @param {TaskOptions} options
     */
    constructor(target, resourceType = RESOURCE_ENERGY, amount = undefined, options = {}) {
        super(NAME, target, options);

        this.settings.oneShot = true;

        this.data.resourceType = resourceType;
        this.data.amount = amount;
    }

    isValidTask() {
        let amount = this.data.amount || 1;
        let resourcesInCarry = this.creep.carry[this.data.resourceType] || 0;
        return resourcesInCarry >= amount;
    }

    isValidTarget() {
        let amount = this.data.amount || 1;
        let target = this.target;
        if (target instanceof Creep) {
            return _.sum(target.carry) <= target.carryCapacity - amount;
        } else if (isStoreStructure(target)) {
            return _.sum(target.store) <= target.storeCapacity - amount;
        } else if (isEnergyStructure(target) && this.data.resourceType === RESOURCE_ENERGY) {
            return target.energy <= target.energyCapacity - amount;
        } else {
            if (target instanceof StructureLab) {
                return (target.mineralType === this.data.resourceType || !target.mineralType) &&
                    target.mineralAmount <= target.mineralCapacity - amount;
            } else if (target instanceof StructureNuker) {
                return this.data.resourceType === RESOURCE_GHODIUM &&
                    target.ghodium <= target.ghodiumCapacity - amount;
            } else if (target instanceof StructurePowerSpawn) {
                return this.data.resourceType === RESOURCE_POWER &&
                    target.power <= target.powerCapacity - amount;
            }
        }
        return false;
    }

    work() {
        return this.creep.transfer(this.target, this.data.resourceType, this.data.amount);
    }

}
