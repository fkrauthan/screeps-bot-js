import Task from "../task";

export const NAME = "harvest";

/**
 * @property {Source|Mineral} target
 */
export default class HarvestTask extends Task {

    /**
     * @param {Source|Mineral} target
     * @param {TaskOptions} options
     */
    constructor(target, options = {}) {
        super(NAME, target, options);
    }

    isValidTask() {
        return _.sum(this.creep.carry) < this.creep.carryCapacity;
    }

    isValidTarget() {
        return this.target.energy !== undefined ? this.target.energy > 0 : this.target.mineralAmount > 0;
    }

    work() {
        return this.creep.harvest(this.target);
    }

}
