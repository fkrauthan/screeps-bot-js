import Task from "../task";

export const NAME = "build";

/**
 * @property {ConstructionSite} target
 */
export default class BuildTask extends Task {

    /**
     * @param {ConstructionSite} target
     * @param {TaskOptions} options
     */
    constructor(target, options = {}) {
        super(NAME, target, options);

        this.settings.targetRange = 3;
        this.settings.workOffRoad = true;
    }

    isValidTask() {
        return this.creep.carry.energy > 0;
    }

    isValidTarget() {
        return this.target && this.target.my && this.target.progress < this.target.progressTotal;
    }

    work() {
        return this.creep.build(this.target);
    }

}
