import Task from "../task";

export const NAME = "upgrade";

/**
 * @property {StructureController} target
 */
export default class UpgradeTask extends Task {

    /**
     * @param {StructureController} target
     * @param {TaskOptions} options
     */
    constructor(target, options = {}) {
        super(NAME, target, options);

        this.settings.targetRange = 3;
        this.settings.workOffRoad = true;
    }

    isValidTask() {
        return this.creep.carry.energy > 0;
    }

    isValidTarget() {
        return this.target && this.target.my;
    }

    work() {
        return this.creep.upgradeController(this.target);
    }

}
