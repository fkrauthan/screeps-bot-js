import Task from "../task";

export const NAME = "transferAll";

/**
 * @property {StructureStorage | StructureTerminal | StructureContainer} target
 * @property {boolean} data.skipEnergy
 */
export default class TransferAllTask extends Task {

    /**
     * @param {StructureStorage | StructureTerminal | StructureContainer} target
     * @param {boolean} skipEnergy
     * @param {TaskOptions} options
     */
    constructor(target, skipEnergy = false, options = {}) {
        super(NAME, target, options);

        this.data.skipEnergy = skipEnergy;
    }

    isValidTask() {
        for (let resourceType in this.creep.carry) {
            if (this.data.skipEnergy && resourceType === RESOURCE_ENERGY) {
                continue;
            }
            let amountInCarry = this.creep.carry[resourceType] || 0;
            if (amountInCarry > 0) {
                return true;
            }
        }
        return false;
    }

    isValidTarget() {
        return _.sum(this.target.store) < this.target.storeCapacity;
    }

    work() {
        for (let resourceType in this.creep.carry) {
            if (this.data.skipEnergy && resourceType === RESOURCE_ENERGY) {
                continue;
            }
            let amountInCarry = this.creep.carry[resourceType] || 0;
            if (amountInCarry > 0) {
                return this.creep.transfer(this.target, resourceType);
            }
        }
        return -1;
    }

}
