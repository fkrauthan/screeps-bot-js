import "./extensions";

import BuildTask from "./impl/build";
import HarvestTask from "./impl/harvest";
import UpgradeTask from "./impl/upgrade";
import RepairTask from "./impl/repair";
import TransferTask from "./impl/transfer";
import TransferAllTask from "./impl/transferAll";

/**
 * @param {[Task]} tasks
 * @param {boolean} setNextPos
 */
export function chain(tasks, setNextPos = true) {
    if (tasks.length === 0) {
        return null;
    }
    if (setNextPos) {
        for (let i = 0; i < tasks.length - 1; i++) {
            tasks[i].options.nextPos = tasks[i + 1].targetPos;
        }
    }

    let task = _.last(tasks);
    tasks = _.dropRight(tasks);
    for (let i = (tasks.length - 1); i >= 0; i--) {
        task = task.fork(tasks[i]);
    }
    return task;
}

/**
 * @param {ConstructionSite} target
 * @param {TaskOptions} options
 * @return {BuildTask}
 */
export function build(target, options = {}) {
    return new BuildTask(target, options);
}

/**
 * @param {StructureController} target
 * @param {TaskOptions} options
 * @return {UpgradeTask}
 */
export function upgrade(target, options = {}) {
    return new UpgradeTask(target, options);
}

/**
 * @param {Source|Mineral} target
 * @param {TaskOptions} options
 * @return {HarvestTask}
 */
export function harvest(target, options = {}) {
    return new HarvestTask(target, options);
}

/**
 * @param {Structure} target
 * @param {TaskOptions} options
 * @return {RepairTask}
 */
export function repair(target, options = {}) {
    return new RepairTask(target, options);
}

/**
 * @param {StructureStorage | StructureTerminal | StructureExtension | StructureContainer | StructureLab | StructureNuker | StructurePowerSpawn | Creep} target
 * @param {ResourceConstant} resourceType
 * @param {?number} amount
 * @param {TaskOptions} options
 * @return {TransferTask}
 */
export function transfer(target, resourceType = RESOURCE_ENERGY, amount = undefined, options = {}) {
    return new TransferTask(target, resourceType, amount, options);
}

/**
 * @param {StructureStorage | StructureTerminal | StructureExtension | StructureContainer | StructureLab | StructureNuker | StructurePowerSpawn | Creep} target
 * @param {boolean} skipEnergy
 * @param {TaskOptions} options
 * @return {TransferAllTask}
 */
export function transferAll(target, skipEnergy = false, options = {}) {
    return new TransferAllTask(target, skipEnergy, options);
}
