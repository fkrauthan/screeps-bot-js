import { deref } from "utils";

import BuildTask, { NAME as buildTaskName } from "./impl/build";
import HarvestTask, { NAME as harvestTaskName } from "./impl/harvest";
import UpgradeTask, { NAME as upgradeTaskName } from "./impl/upgrade";
import RepairTask, { NAME as repairTaskName } from "./impl/repair";
import TransferTask, { NAME as transferTaskName } from "./impl/transfer";
import TransferAllTask, { NAME as transferAllTaskName } from "./impl/transferAll";

/**
 * @typedef {Object} TaskProto
 * @property {string} name
 * @property {string} _creep
 * @property _target
 * @property {?TaskProto} _parent
 * @property {number} tick
 * @property {TaskOptions} options
 */

/**
 * @param {TaskProto} proto
 * @return {Task}
 */
export default function create(proto) {
    const name = proto.name;
    const target = deref(proto._target.ref);
    let task = undefined;

    switch (name) {
        case buildTaskName:
            task = new BuildTask(target);
            break;
        case harvestTaskName:
            task = new HarvestTask(target);
            break;
        case upgradeTaskName:
            task = new UpgradeTask(target);
            break;
        case repairTaskName:
            task = new RepairTask(target);
            break;
        case transferTaskName:
            task = new TransferTask(target);
            break;
        case transferAllTaskName:
            task = new TransferAllTask(target);
            break;
        default:
            console.log(`Found invalid task "${name}" for creep "${proto._creep}"`);
    }

    if (task) {
        task.proto = proto;
    }
    return task;
}
