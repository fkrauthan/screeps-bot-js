import {deref, derefRoomPosition} from "utils";
import factory from "./factory";

/**
 * @typedef {Object} TaskOptions
 * @property {?boolean} blind
 * @property nextPos
 * @property {MoveOptions} moveOptions
 */

/**
 * @property {RoomPosition} _targetPost
 */
export default class Task {

    _creep = "";
    _parent = undefined;

    tick = Game.time;

    settings = {
        targetRange: 1,
        workOffRoad: false,
        oneShot: false,
        timeout: Infinity,
        blind: true
    };

    data = {};

    /**
     * @param {string} taskName
     * @param {object} target
     * @param {TaskOptions} options
     */
    constructor(taskName, target, options) {
        this.taskName = taskName;
        this.options = options;

        if (target) {
            this._target = {
                ref: target.ref,
                _pos: target.pos,
            };
        } else {
            this._target = {
                ref: '',
                _pos: {
                    x: -1,
                    y: -1,
                    roomName: "",
                }
            };
        }
    }

    /**
     * @return {TaskProto}
     */
    get proto() {
        return {
            name: this.taskName,
            _creep: this._creep,
            _target: this._target,
            _parent: this._parent,
            tick: this.tick,
            options: this.options,
            data: this.data,
        };
    }

    /**
     * @param {TaskProto} taskProto
     */
    set proto(taskProto) {
        this._creep = taskProto._creep;
        this._target = taskProto._target;
        this._parent = taskProto._parent;
        this.tick = taskProto.tick;
        this.options = taskProto.options;
        this.data = taskProto.data;
    }

    /**
     * @param {Creep} creep
     */
    set creep(creep) {
        this._creep = creep.name;
    }

    /**
     * @return {Creep}
     */
    get creep() {
        return Game.creeps[this._creep];
    }

    /**
     * @return {Task | null}
     */
    get parent() {
        const parent = this._parent ? factory(this._parent) : null;
        if (parent) {
            parent.creep = this.creep;
        }
        return parent;
    }

    /**
     * @param {Task | null} parentTask
     */
    set parent(parentTask) {
        this._parent = parentTask ? parentTask.proto : null;

        if (this._creep && this.creep) {
            this.creep.task = this;
        }
    }

    /**
     * @return {RoomObject | null}
     */
    get target() {
        return deref(this._target.ref);
    }

    /**
     * @return {RoomPosition}
     */
    get targetPos() {
        if (!this._targetPos) {
            if (this.target) {
                this._target._pos = this.target.pos;
            }
            this._targetPos = derefRoomPosition(this._target._pos);
        }
        return this._targetPos;
    }

    /**
     * @param {number} range
     * @return {ScreepsReturnCode}
     */
    moveToTarget(range = this.settings.targetRange) {
        if (this.options.moveOptions && !this.options.moveOptions.range) {
            this.options.moveOptions.range = range;
        }
        return this.creep.travelTo(this.targetPos, this.options.moveOptions);
    }

    /**
     * @return {ScreepsReturnCode | undefined}
     */
    moveToNextPos() {
        if (this.options.nextPos) {
            let nextPos = derefRoomPosition(this.options.nextPos);
            return this.creep.travelTo(nextPos);
        }
    }

    /**
     * @return {boolean}
     */
    get isWorking() {
        return this.creep.pos.inRangeTo(this.targetPos, this.settings.targetRange) && !this.creep.pos.isEdge;
    }

    /**
     * @param {Task} newTask
     * @return {Task}
     */
    fork(newTask) {
        newTask.parent = this;
        if (this.creep) {
            this.creep.task = newTask;
        }
        return newTask;
    }

    /**
     * @return {boolean}
     */
    isValid() {
        let validTask = false;
        if (this.creep) {
            validTask = this.isValidTask() && Game.time - this.tick < this.settings.timeout;
        }
        let validTarget = false;
        if (this.target) {
            validTarget = this.isValidTarget();
        } else if ((this.settings.blind || this.options.blind) && !Game.rooms[this.targetPos.roomName]) {
            // If you can't see the target's room but you have blind enabled, then that's okay
            validTarget = true;
        }
        // Return if the task is valid; if not, finalize/delete the task and return false
        if (validTask && validTarget) {
            return true;
        } else {
            // Switch to parent task if there is one
            this.finish();
            return this.parent ? this.parent.isValid() : false;
        }
    }

    /**
     * @return {ScreepsReturnCode | undefined}
     */
    run() {
        if (this.isWorking) {
            if (this.settings.workOffRoad) {
                this.creep.park(this.targetPos, true);
            }
            let result = this.work();
            if (this.settings.oneShot && result === OK) {
                this.finish();
            }
            return result;
        } else {
            this.moveToTarget();
        }
    }

    finish() {
        this.moveToNextPos();
        if (this.creep) {
            this.creep.task = this.parent;
        } else {
            console.log(`No creep executing ${this.taskName}! Proto: ${JSON.stringify(this.proto)}`);
        }
    }

}
