import factory from "./factory";

Object.defineProperty(Creep.prototype, 'task', {
    /**
     * @return {Task | null}
     */
    get() {
        if (!this._task) {
            let protoTask = this.memory.task;
            this._task = protoTask ? factory(protoTask) : null;
            if (this._task) {
                this._task.creep = this;
            }
        }
        return this._task;
    },
    /**
     * @param {?Task} task
     */
    set(task) {
        this.memory.task = task ? task.proto : null;
        if (task) {
            task.creep = this;
        }
        this._task = task;
    },
});

Creep.prototype.run = function () {
    if (this.task) {
        return this.task.run();
    }
};

Object.defineProperties(Creep.prototype, {
    'hasValidTask': {
        /**
         * @return {boolean}
         */
        get() {
            return this.task && this.task.isValid();
        }
    },
    'isIdle': {
        /**
         * @return {boolean}
         */
        get() {
            return !this.hasValidTask;
        }
    }
});
