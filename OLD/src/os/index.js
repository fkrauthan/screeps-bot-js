import Kernel from "./Kernel";
import Process from "./Process";
import CreepProcess from "./coreProcesses/CreepProcess";

export {
    Process,
    CreepProcess,
};
export default Kernel;
