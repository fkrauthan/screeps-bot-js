import Scheduler from "./Scheduler";
import Process from "./Process";
import coreProcessFactory from "./coreProcesses/factory";

/**
 * @typedef {object} ExecutionStatistic
 * @property {string} name
 * @property {number} cpu
 * @property {string} type
 * @property {boolean} failed
 */

const MAX_PID = 32768;

/**
 * @property {number} limit
 * @property {function} factory
 */
export default class Kernel {

    stats = {
        bootUsage: 0,
        pidCalcUsage: 0,
        shutdownUsage: 0,
        executions: [],
        suspendCount: 0,
    };

    nextPID = Memory._nextPID || 1;

    scheduler = new Scheduler(this);

    /**
     * @type {Object.<string, Process>}
     */
    processTable = {};

    /**
     * @type {Object.<string, number>}
     */
    tagsToProcessTable = {};

    /**
     * @param {function} factory
     */
    constructor(factory) {
        this.factory = factory;
    }

    boot() {
        const cpu = Game.cpu.getUsed();

        if (this.new) {
            // If freshly booted restore from memory
            this.new = false;
            this._loadProcessTableFromMemory();
        } else {
            // If used old copy clean completed tasks
            this._cleanExistingProcessTable();
        }

        this._calculateCPULimit();
        this.stats = {
            bootUsage: 0,
            pidCalcUsage: 0,
            shutdownUsage: 0,
            executions: [],
            suspendCount: 0,
        };

        this.scheduler.prepare();

        this.stats.bootUsage = Game.cpu.getUsed() - cpu;
    }

    run() {
        this._handleSuspendedTasks();

        let process = null;
        while(this.isUnderLimit() && (process = this.scheduler.getNextProcess())){
            kernel._runProcess(process);
        }
    }

    shutdown() {
        const cpu = Game.cpu.getUsed();

        // Store process table in memory
        Memory._processTable = Object.values(this.processTable)
            .filter(({ completed }) => !completed)
            .map(({ prototype }) => prototype);

        this.stats.shutdownUsage = Game.cpu.getUsed() - cpu;
    }

    /**
     * @param {string} type
     * @param {number} priority
     * @param {?{}} data
     * @param {{}} options
     * @param {?string} options.parent
     * @param {?tag} options.tag
     *
     * @return {number}
     */
    addProcess(type, priority, data = {}, options = { parent: undefined, tag: undefined, }) {
        const pid = this._getNextPID();
        return this.addProcessWithPID(type, pid, priority, data, parent);
    }

    /**
     * @param {string} type
     * @param {number} pid
     * @param {number} priority
     * @param {?{}} data
     * @param {{}} options
     * @param {?string} options.parent
     * @param {?tag} options.tag
     *
     * @return {number}
     */
    addProcessWithPID(type, pid, priority, data = {}, options = { parent: undefined, tag: undefined, }) {
        this.processTable[pid] = this._createProcessInstance({
            pid,
            type,
            priority,
            data,
            parent,
            tag,
        });
        this.scheduler.reschedule();

        if (tag) {
            this.tagsToProcessTable[tag] = pid;
        }

        return pid;
    }

    addProcessIfNotExist(type, tag, priority, data = {}, parent = undefined) {
        if (!this.hasProcessWithTag(tag)) {
            return this.addProcess(type, priority, data, { parent, tag });
        }
        return this.getProcessByTag(tag);
    }

    addProcessWithPIDIfNotExist(type, pid, priority, data = {}, parent = undefined) {
        if (!this.hasProcess(pid)) {
            return this.addProcessWithPID(type, pid, priority, data, parent);
        }
        return pid;
    }

    /**
     * @param {number} pid
     * @return {boolean}
     */
    hasProcess(pid) {
        return !!this.processTable[pid];
    }

    /**
     * @param {string} tag
     * @return {boolean}
     */
    hasProcessWithTag(tag) {
        return !!this.tagsToProcessTable[tag];
    }

    /**
     * @param {number} pid
     * @return {Process}
     */
    getProcess(pid) {
        return this.processTable[pid];
    }

    /**
     * @param {string} tag
     * @return {Process}
     */
    getProcessByTag(tag) {
        return this.processTable[this.tagsToProcessTable[tag]];
    }

    /**
     * @param {number} pid
     * @return {Process[]}
     */
    getChildProcesses(pid) {
        return Object.values(this.processTable)
            .filter(({ parent }) => parent && parent.pid === pid);
    }

    _getNextPID() {
        const cpu = Game.cpu.getUsed();

        let pid = this.nextPID;
        while(this.hasProcess(pid)) {
            pid += 1;
            if (pid > MAX_PID) {
                pid = 1;
            }
        }

        this.nextPID = pid + 1;
        Memory._nextPID = this.nextPID;

        this.stats.pidCalcUsage = Game.cpu.getUsed() - cpu;
        return pid;
    }

    /**
     * @param {Process} process
     * @private
     */
    _runProcess(process) {
        let cpuUsed = Game.cpu.getUsed();
        let failed = false;

        try{
            if (process.completed === false) {
                process.run();
            }
        } catch (e) {
            console.log(`Process ${process.pid} failed: ${e.message}`, e);
            failed = true
        }

        this.stats.executions.push({
            pid: process.pid,
            cpu: Game.cpu.getUsed() - cpuUsed,
            type: process.type,
            failed: failed,
            completed: process.completed,
        });
        process.lastExecution = Game.time;
    }

    _handleSuspendedTasks() {
        Object.values(this.processTable)
            .filter(({ suspended }) => !(suspended === false))
            .forEach((process) => {
                this.stats.suspendCount += 1;
                if(typeof process.suspended === "number") {
                    process.suspended -= 1;
                    if(process.suspended === 0) {
                        process.resume();
                    }
                } else if (process.suspendedForPID !== undefined) {
                    if (!this.hasProcess(process.suspendedForPID)) {
                        process.resume();
                    }
                }
            });
    }

    /**
     * @return {boolean}
     */
    isUnderLimit() {
        return Game.cpu.getUsed() < this.limit;
    }

    _calculateCPULimit() {
        let bucketCeiling = 9500;
        let bucketFloor = 2000;
        let cpuMin = 0.6;

        // Support the simulator
        // if(Game.cpu.limit === undefined){
        this.limit = 1000;
        //     return;
        // }

        // if(Game.cpu.bucket > bucketCeiling){
        //     this.limit = Game.cpu.tickLimit - 10;
        // }else if(Game.cpu.bucket < 1000){
        //     this.limit = Game.cpu.limit * 0.4;
        // }else if(Game.cpu.bucket < bucketFloor){
        //     this.limit = Game.cpu.limit * cpuMin;
        // }else{
        //     let bucketRange = bucketCeiling - bucketFloor;
        //     let depthInRange = (Game.cpu.bucket - bucketFloor) / bucketRange;
        //     let minAssignable = Game.cpu.limit * cpuMin;
        //     let maxAssignable = Game.cpu.tickLimit - 15;
        //     // this.limit = Math.round(minAssignable + this.sigmoidSkewed(depthInRange) * (maxAssignable - minAssignable));
        // }
    }

    /**
     * @param {ProcessPrototype} proto
     * @return {Process}
     *
     * @private
     */
    _createProcessInstance(proto) {
        return coreProcessFactory(proto, this) ||
            this.factory(proto, this) ||
            new Process(proto.type, proto, this);
    }

    _loadProcessTableFromMemory() {
        if (Memory._processTable) {
            this.processTable = {};
            this.tagsToProcessTable = {};
            Object.values(Memory._processTable)
                .forEach((proto) => {
                    this.processTable[proto.pid] = this._createProcessInstance(proto);
                    if (proto.tag) {
                        this.tagsToProcessTable[proto.tag] = proto.pid;
                    }
                });
        }
    }

    _cleanExistingProcessTable() {
        Object.values(this.processTable)
            .forEach((process) => {
                if (process.completed) {
                    delete this.processTable[process.pid];
                    if (process.tag) {
                        delete this.tagsToProcessTable[process.tag];
                    }
                }
            });
    }

}
