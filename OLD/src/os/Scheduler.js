/**
 * @property {Kernel} kernel
 */
export default class Scheduler {

    stats = {
        prepareUsage: 0,
        runUsage: 0,
    };

    /**
     * @type {string[]}
     */
    processesToRun = [];

    /**
     * @param {Kernel} kernel
     */
    constructor(kernel) {
        this.kernel = kernel;
    }

    prepare() {
        const cpu = Game.cpu.getUsed();

        this.processesToRun = [];

        this.stats.prepareUsage = Game.cpu.getUsed() - cpu;
    }

    reschedule() {
        this.processesToRun = [];
    }

    getNextProcess() {
        const cpu = Game.cpu.getUsed();

        if(this.processesToRun.length === 0) {
            this.processesToRun = Object.values(this.kernel.processTable)
                .filter(({ lastExecution, suspended }) => lastExecution !== Game.time && suspended === false)
                .sort(({ priority: taskPriorityA, lastExecution: taskALastExecution }, { priority: taskPriorityB, lastExecution: taskBLastExecution }) => taskPriorityA === taskPriorityB ? taskALastExecution - taskBLastExecution : taskPriorityB - taskPriorityA)
                .map(({ pid })=> pid);
        }
        const pid = this.processesToRun.shift();
        this.stats.runUsage += Game.cpu.getUsed() - cpu;

        return this.kernel.processTable[pid];
    }

}
