import Process from "../Process";

export const TYPE = "creepMemoryClean";
export default class CreepMemoryCleanProcess extends Process {

    /**
     * @param {ProcessPrototype} proto
     * @param {Kernel} kernel
     */
    constructor(proto, kernel) {
        super(TYPE, proto, kernel);
    }

    run() {
        // Start room master process for each room
        Object.keys(Memory.creeps)
            .filter((name) => !Game.creeps[name])
            .forEach((name) => delete Memory.creeps[name]);

        // Run this task every 100 ticks
        this.suspendForTicks(100);
    }

}
