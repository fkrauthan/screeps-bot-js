import { chain, harvest, upgrade, build, repair } from "tasks";

import Traveler from "navigation";

export default class RoomManager {

    /**
     * @type {string}
     */
    roomName = undefined;

    /**
     * @type {[Creep]}
     */
    creeps = [];

    /**
     * @type {[string]}
     */
    sourceIds = [];

    /**
     * @type {string}
     */
    controllerId = undefined;

    /**
     * @type {string}
     */
    spawnId = undefined;

    /**
     * @type {?Room}
     * @private
     */
    _roomInstance = undefined;

    /**
     * @type {?StructureSpawn}
     * @private
     */
    _spawnInstance = undefined;

    /**
     * @type {?StructureController}
     * @private
     */
    _controllerInstance = undefined;

    /**
     * @param {Room} room
     * @param {StructureSpawn} spawn
     */
    constructor(room, spawn) {
        this.roomName = room.name;

        this.sourceIds = room.find(FIND_SOURCES).map(source => source.id);
        this.controllerId = room.controller.id;
        this.spawnId = spawn.id;

        this._roomInstance = room;
        this._spawnInstance = spawn;
        this._controllerInstance = room.controller;
    }

    /**
     * @return {?Room}
     */
    get room() {
        if (!this._roomInstance) {
            this._roomInstance = Game.rooms[this.roomName];
        }
        return this._roomInstance;
    }

    /**
     * @return {?StructureSpawn}
     */
    get spawn() {
        if (!this._spawnInstance) {
            this._spawnInstance = Game.getObjectById(this.spawnId);
        }
        return this._spawnInstance;
    }

    /**
     * @return {?StructureController}
     */
    get controller() {
        if (!this._controllerInstance) {
            this._controllerInstance = Game.getObjectById(this.controllerId);
        }
        return this._controllerInstance;
    }

    /**
     * @param {number} index
     * @return {Source}
     */
    getSource(index) {
        return Game.getObjectById(this.sourceIds[index]);
    }

    /**
     * @param {boolean} clearInstances
     */
    init(clearInstances = true) {
        this.creeps = Object.values(Game.creeps).filter(creep => creep.pos.roomName === this.roomName);

        if (clearInstances) {
            this._roomInstance = undefined;
            this._spawnInstance = undefined;
            this._controllerInstance = undefined;
        }
    }

    spawnCreeps() {
        // Run low level quick upgrade code
        if (this.controller.level <= 3) {
            // Always spawn a simple bot
            if (this.spawn.spawnCreep([CARRY, WORK, MOVE], `J${Game.time}`) === OK) {
                console.log(`Spawned new basic creep J${Game.time}`);
            }
        }
    }

    tick() {
        // Build roads as soon as we have some creeps
        if (this.creeps.length > 3 && this.room.memory.sourceRoadsBuild || false === false) {
            this.room.memory.sourceRoadsBuild = true;

            this.sourceIds.forEach((id, i) => {
                const ret = Traveler.findTravelPath(this.getSource(i), this.controller);
                ret.path.forEach(pos => this.room.createConstructionSite(pos, STRUCTURE_ROAD));
            });
        }

        // Run low level quick upgrade code
        if (this.controller.level <= 3) {
            if (!this.nextSourceIndex) {
                this.nextSourceIndex = 0;
            }

            const constructionSites = this.room.find(FIND_CONSTRUCTION_SITES);
            const structuresThatNeedRepair = this.room.find(FIND_MY_STRUCTURES, {
                filter: s => s.hits < s.hitsMax && s.structureType !== STRUCTURE_WALL,
            });
            this.creeps
                .filter((creep) => creep.isIdle)
                .forEach((creep) => {
                    if (constructionSites.length > 0 && (!this.createdConstructionCreep || this.createdConstructionCreep < 3)) {
                        this.createdConstructionCreep = (this.createdConstructionCreep || 0) + 1;

                        creep.task = chain([harvest(this.getSource(this.nextSourceIndex)), build(constructionSites[0])]);
                    } else if (structuresThatNeedRepair.length > 0 && (!this.createdRepairCreep || this.createdConstructionCreep < 3)) {
                        this.createdConstructionCreep = (this.createdConstructionCreep || 0) + 1;

                        creep.task = chain([harvest(this.getSource(this.nextSourceIndex)), repair(structuresThatNeedRepair[0])]);
                    } else {
                        creep.task = chain([harvest(this.getSource(this.nextSourceIndex)), upgrade(this.controller)]);
                    }

                    this.nextSourceIndex += 1;
                    if (this.nextSourceIndex >= this.sourceIds.length) {
                        this.nextSourceIndex = 0;
                    }
                })
        }
    }

}
