import "./extensions";
import "navigation";

import {onlyOneTickAdvanced, optimizeMemory} from "./utils/memory";
import Kernel from "os";

import processFactory from "./processes/factory";

export default function coreLoop() {
    // Check if we only advanced on tick on this server
    const didOnlyOneTickAdvance = onlyOneTickAdvanced();

    // Optimize memory
    optimizeMemory(didOnlyOneTickAdvance);

    // Initialize Kernel
    /** @type {Kernel} kernel */
    const kernel = (global.kernel && didOnlyOneTickAdvance) ? global.kernel : new Kernel(processFactory);
    global.kernel = kernel;

    // Add init task if it does not exist
    kernel.addProcessWithPIDIfNotExist("init", 1, 99);

    // Run kernel
    kernel.boot();
    kernel.run();
    kernel.shutdown();

    // Print out run statistics
    // console.log(`[${Game.time}] Executed Processes ${JSON.stringify(kernel.execStats)}`);
    console.log(`[${Game.time}] Execution Stats: ${JSON.stringify(kernel.stats)}`)
}
