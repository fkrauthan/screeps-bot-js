/**
 * @return {boolean}
 */
export function onlyOneTickAdvanced() {
    let res = false;
    if(global.lastTick && Game.time === (global.lastTick + 1)) {
        res = true;
    }
    global.lastTick = Game.time;
    return res;
}

/**
 * @param {boolean} reUseIfAvailable
 */
export function optimizeMemory(reUseIfAvailable) {
    if(reUseIfAvailable && global.lastMemory) {
        // noinspection JSAnnotator
        delete global.Memory;
        // noinspection JSUnresolvedVariable
        global.Memory = global.lastMemory;
        RawMemory._parsed = global.lastMemory;
    } else {
        // noinspection BadExpressionStatementJS
        Memory;
        global.lastMemory = RawMemory._parsed;
    }
}
