import { CreepProcess } from "os";

export const TYPE = "upgradeRole";
export default class UpgradeRoleProcess extends CreepProcess {

    /**
     * @param {ProcessPrototype} proto
     * @param {Kernel} kernel
     */
    constructor(proto, kernel) {
        super(TYPE, proto, kernel);
    }

    run() {
        const creep = this.creep;
        if (!creep) {
            this.finish();
        }

        if (creep.carry.energy > 0) {
            this.forkAndSuspend('upgrade', this.priority - 1, {
                creep: creep.name,
                target: creep.room.controller.ref,
            });
        } else {
            this.forkAndSuspend('harvest', this.priority - 1, {
                creep: creep.name,
                target: this.data.sourceId,
            });
        }
    }

}
