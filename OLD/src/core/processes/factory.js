import InitProcess, { TYPE as TYPE_INIT} from "./InitProcess";
import RoomMasterProcess, { TYPE as TYPE_ROOM_MASTER} from "./RoomMasterProcess";
import UpgradeRoleProcess, { TYPE as TYPE_UPGRADE_ROLE} from "./UpgradeRoleProcess";

const processTypes = {
    [TYPE_INIT]: InitProcess,
    [TYPE_ROOM_MASTER]: RoomMasterProcess,

    [TYPE_UPGRADE_ROLE]: UpgradeRoleProcess,
};

export default function factory(proto, kernel) {
    if (!!processTypes[proto.type]) {
        return new processTypes[proto.type](proto, kernel);
    }
    return null;
}
