Orca Screeps AI
===============

This is my screeps bot written with Javascript.


Install
-------

Run `yarn install` to install all dependencies. After that you can run `yarn start`
to compile the bot. All files are going to end up in the `./dist` folder.
 