import fs from 'fs';
import path from 'path';
import { promisify } from 'util';

// import progress from 'rollup-plugin-progress';
import screeps from './rollup/rollup-plugin-screeps';
import notify from 'rollup-plugin-notify';
import babel from 'rollup-plugin-babel';


const readdir = promisify(fs.readdir);


const IGNORE_CIRCULAR_DEPENDENCIES = {
    tasks: [
        "factory.js",
    ],
};

async function buildRollupConfig() {
    const sourceRoot = `${__dirname}/src`;
    const distRoot = `${__dirname}/dist`;

    const rollupPlugins = [
        notify(),
        // progress(),
        babel({
            exclude: 'node_modules/**'
        }),
        screeps({
            server: process.env.SCREEPS_SERVER || 'localhost:21025',
            branch: process.env.SCREEPS_BRANCH || 'default',
        }),
    ];

    const modules = (await readdir(sourceRoot))
        .filter((file) => fs.lstatSync(`${sourceRoot}/${file}`).isDirectory())
        .filter((file) => fs.lstatSync(`${sourceRoot}/${file}/index.js`).isFile());

    const modulesConfigs = modules
        .map((module) => ({
            input: `${sourceRoot}/${module}/index.js`,
            plugins: rollupPlugins,
            output: {
                file: `${distRoot}/${module}.js`,
                format: 'cjs',
                exports: 'named',
            },
            onwarn: (warning, defaultOnWarnHandler) => {
                if (warning.code === "CIRCULAR_DEPENDENCY" && IGNORE_CIRCULAR_DEPENDENCIES.hasOwnProperty(module)) {
                    const normalizedPath = warning.importer.replace(`${path.join("src", module)}${path.sep}`, "");
                    if (IGNORE_CIRCULAR_DEPENDENCIES[module].indexOf(normalizedPath) !== -1) {
                        return;
                    }
                }
                defaultOnWarnHandler(warning);
            },
            external: modules,
        }));

    return [
        ...modulesConfigs,
        {
            input: `${sourceRoot}/main.js`,
            plugins: rollupPlugins,
            output: {
                file: `${distRoot}/main.js`,
                format: 'cjs',
            },
            external: modules,
        }
    ]
}

export default buildRollupConfig();
