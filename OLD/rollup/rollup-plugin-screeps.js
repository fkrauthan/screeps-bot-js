import os from "os";
import path from "path";

import fs from "fs";
import { promisify } from 'util';

const writeFile = promisify(fs.writeFile);
const lstat = promisify(fs.lstat);
const mkdir = promisify(fs.mkdir);


function formatScreepsServerName(server) {
    return server
        .replace(/\./g, '_')
        .replace(/:/g, '___');
}

function detectScreepsPath(server, branch) {
    if (process.platform === 'win32') {
        return path.join(os.homedir(), 'AppData', 'Local', 'Screeps', 'scripts', formatScreepsServerName(server), branch);
    }
    if (process.platform === 'darwin') {
        return path.join(os.homedir(), 'Library', 'Application Support', 'Screeps', 'scripts', formatScreepsServerName(server), branch);
    }

    throw new Error(`${process.platform} not supported at the moment!`);
}

export default function screepsPlugin ({ server, branch }) {
    const destinationPath = detectScreepsPath(server, branch);

    return {
        name: 'screeps',

        async generateBundle(outputOptions, bundle, isWrite) {
            if (isWrite) {
                try {
                    await lstat(destinationPath);
                } catch (e) {
                    await mkdir(destinationPath);
                }

                const fileWrites = Object.values(bundle)
                    .map(({fileName, code}) => writeFile(path.join(destinationPath, fileName), code));
                return Promise.all(fileWrites);
            }
        },
    };
}
